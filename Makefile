APP_NAME=jquery
PACKAGE=acf-$(APP_NAME)
VERSION=0.4.3

APP_DIST=\
	*.js\
	widgets/*.js\
	parsers/*.js\

STATIC_DIST=\
	tablesorter/*\

EXTRA_DIST=README Makefile config.mk

DISTFILES=$(APP_DIST) $(STATIC_DIST) $(EXTRA_DIST)

TAR=tar

P=$(PACKAGE)-$(VERSION)
tarball=$(P).tar.bz2
install_dir=$(DESTDIR)/$(wwwdir)/js
static_dir=$(DESTDIR)/$(staticdir)/tablesorter

all:
clean:
	rm -rf $(tarball) $(P)

dist: $(tarball)

install:
	mkdir -p "$(install_dir)"
	for i in $(APP_DIST); do\
		dest=`dirname "$(install_dir)/$$i"`;\
		mkdir -p "$$dest";\
		cp "$$i" "$$dest";\
	done
	mkdir -p "$(static_dir)"
	cp -a $(STATIC_DIST) "$(static_dir)"

$(tarball):	$(DISTFILES)
	rm -rf $(P)
	mkdir -p $(P)
	cp -a $(DISTFILES) $(P)
	$(TAR) -jcf $@ $(P)
	rm -rf $(P)

# target that creates a tar package, unpacks is and install from package
dist-install: $(tarball)
	$(TAR) -jxf $(tarball)
	$(MAKE) -C $(P) install DESTDIR=$(DESTDIR)
	rm -rf $(P)

include config.mk

.PHONY: all clean dist install dist-install
